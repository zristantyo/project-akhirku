 <!--  page-wrapper -->
        <div id="page-wrapper">

            <div class="row">
                <!-- Page Header -->
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!--End Page Header -->
            </div>


            <div class="row">
                <!-- Welcome -->
                <div class="col-lg-12">
                    <div class="alert alert-info">
                        <i class="fa fa-folder-open"></i><b>&nbsp;Hello ! </b>Welcome Back <b>
                    </div>
                </div>
                <!--end  Welcome -->
            </div>

            <div class="row">
                <div class="col-lg-8">

                <!--Area chart example -->
                <div class="panel panel-primary">
                        <div class="panel-heading">
                            <i class=""></i>Dashboard
                            <div class="pull-right">
                                <div class="btn-group">
                                    
                                    
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                        <a class="quick-button metro yellow span3">
    <i class="	fa fa-id-card fa-3x"></i>
    
    <h1 ><?php $query = $this->db->query('SELECT * FROM tbl_customer'); echo $query->num_rows();?></h1>
    <h4>Users</h4>
</a>
<a class="quick-button metro blue span3">
    <i class="fa fa-shopping-cart fa-3x"></i>
    <h1><?php $query = $this->db->query('SELECT * FROM tbl_order'); echo $query->num_rows();?></h1>
    <h4>Orders</h4>
</a>

<a class="quick-button metro red span3">
    <i class="icon-th-list"></i>
    <h1><?php $query = $this->db->query('SELECT * FROM tbl_category'); echo $query->num_rows();?></h1>
    <h4>Categories</h4>
</a>

<a class="quick-button metro pink span3">
    <i class="icon-tag"></i>
    <h1><?php $query = $this->db->query('SELECT * FROM tbl_brand'); echo $query->num_rows();?></h1>
    <h4>Brands</h4>
</a>
                        </div>


                    
                        

                </div>
                    <!--End area chart example -->
                    

                </div>

                <div class="col-lg-4">
                    <div class="panel panel-primary text-center no-boder">
                        <div class="panel-body yellow">
                            <i class="fa fa-bar-chart-o fa-3x"></i>
                            <h3>20,741 </h3>
                        </div>
                        <div class="panel-footer">
                            <span class="panel-eyecandy-title">Daily User Visits
                            </span>
                        </div>
                    </div>
                    <div class="panel panel-primary text-center no-boder">
                        <div class="panel-body blue">
                            <i class="fa fa-pencil-square-o fa-3x"></i>
                            <h3>110 </h3>
                        </div>
                        <div class="panel-footer">
                            <span class="panel-eyecandy-title">Pending Orders Found
                            </span>
                        </div>
                    </div>
                    
                    <div class="panel panel-primary text-center no-boder">
                        <div class="panel-body red">
                            <i class="fa fa-thumbs-up fa-3x"></i>
                            <h3>2,700 </h3>
                        </div>
                        <div class="panel-footer">
                            <span class="panel-eyecandy-title">New User Registered
                            </span>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col-lg-4">
                    
                </div>
                <div class="col-lg-4">
                    
                </div>
                <div class="col-lg-4">
                    
                </div>
            </div>
        </div>
        <!-- end page-wrapper -->